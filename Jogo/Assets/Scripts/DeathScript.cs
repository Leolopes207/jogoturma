﻿using UnityEngine;
using System.Collections;

public class DeathScript : MonoBehaviour {

    public GameObject deathText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnDestroy()
    {
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        Instantiate(deathText, new Vector3(camera.transform.position.x, camera.transform.position.y, camera.transform.position.z+1), Quaternion.identity);
    }
}
