﻿using UnityEngine;
using System.Collections;

public class ChaoMortal : MonoBehaviour
{

    public GameObject player;
    public Animator animator;
    public PlayerScript playerScript;

    // Use this for initialization
    void Start()
    {
        PlayerScript playerScript = player.GetComponent<PlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (player != null)
            {
                playerScript.enabled = false;
                player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                GameObject.FindGameObjectWithTag("Pause").GetComponent<PauseMenuScript>().enabled = false;
                StartCoroutine(Explosion());
            }
        }
    }

    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(1);
        Object.Destroy(player);
    }
}