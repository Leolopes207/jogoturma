﻿using UnityEngine;
using System.Collections;

public class ArthurBoss : MonoBehaviour
{
    public float start;
    public float end;
    private float y;
    private float x;
    private float aux = -1;
    public float speed;
    public GameObject player;
    public GameObject shot;
    private float shotDelay;
    private bool esquerda = false;
    private bool direita = false;
    public int vida;
    public Sprite change;
    public Sprite newShot;
    public Sprite atual;
    public bool morto = false;
    public bool morrendo = false;
    public Animator anim;
    public float velocidade;
    public GameObject ponte;
    // Use this for initialization
    void Start()
    {
        shot.gameObject.GetComponent<SpriteRenderer>().sprite = atual;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!morrendo)
        {
            if (esquerda)
            {
                x = transform.position.x;
                x += speed * Time.deltaTime * aux;
                transform.position = new Vector3(x, transform.position.y, transform.position.z);
                if (transform.position.x <= 15)
                {
                    esquerda = false;
                    direita = true;
                    aux = 1;
                }
            }
            else {
                if (direita)
                {
                    x = transform.position.x;
                    x += speed * Time.deltaTime * aux;
                    transform.position = new Vector3(x, transform.position.y, transform.position.z);
                    if (transform.position.x >= 24)
                    {
                        direita = false;
                        aux = -1;
                    }
                }
                else {
                    y = transform.position.y;
                    if (y <= start)
                    {
                        aux = 1;
                    }
                    if (y >= end)
                    {
                        esquerda = true;
                        aux = -1;
                    }
                    y += speed * Time.deltaTime * aux;
                    transform.position = new Vector3(transform.position.x, y, transform.position.z);
                }
            }
            if (Time.time > shotDelay)
                if(player != null)
                    Ataque();
        } else
        {
            if (morto)
            {
                y += speed * Time.deltaTime * -1;
                transform.position = new Vector3(transform.position.x, y, transform.position.z);
                transform.Rotate(new Vector3(y*2, y/2, y*2));
            }
        }
    }

    void Ataque()
    {
        if(player.transform.position.x > transform.position.x)
            Instantiate(shot, new Vector3(transform.position.x + 2, transform.position.y - 1f, transform.position.z), Quaternion.identity);
        else 
            Instantiate(shot, new Vector3(transform.position.x - 3, transform.position.y - 1f, transform.position.z), Quaternion.identity);
        shotDelay = Time.time + 1f;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Shot")
        {
            vida--;
            if (vida <= 0)
            {
                StartCoroutine(Morrendo());
            }
            else {
                if (vida == 2)
                {
                    
                    anim.Play("BossParado2");
                    shot.gameObject.GetComponent<SpriteRenderer>().sprite = newShot;
                }
            }
        }
    }

    IEnumerator Morrendo()
    {
        morrendo = true;
        anim.Play("BossSemiDeath"); 
        yield return new WaitForSeconds(2);
        StartCoroutine(Morto());
    }

    IEnumerator Morto()
    {
        morto = true;
        anim.Play("BossParado");
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }

    void Aparecendo()
    {

    }

    void OnDestroy()
    {
        if (player != null)
        {
            Instantiate(ponte, new Vector3(24.43f, 8.6f, 0f), Quaternion.identity);
            player.GetComponent<PlayerScript>().enabled = false;
        }
    }
}
