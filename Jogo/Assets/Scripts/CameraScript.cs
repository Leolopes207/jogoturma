﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
    private Vector2 velocity;
    public GameObject player;
    private float posx;
    private float posy;
    public float mapStart;
    public float mapEnd;
    
    void Start()
    {
        float targetaspect = 1920.0f / 1080.0f;

        // determine the game window's current aspect ratio
        float windowaspect = (float)Screen.width / (float)Screen.height;

        // current viewport height should be scaled by this amount
        float scaleheight = windowaspect / targetaspect;

        // obtain camera component so we can modify its viewport
        Camera camera = GetComponent<Camera>();

        // if scaled height is less than current height, add letterbox
        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            camera.rect = rect;
        }
        else // add pillarbox
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            camera.rect = rect;
        }
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void LateUpdate()
    {
        if (player != null)
        {
            if (player.transform.position.x > mapStart)
            {
                if (player.transform.position.x < mapEnd)
                {
                    posx = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x, 0);
                } else
                {
                    posx = Mathf.SmoothDamp(mapEnd, mapEnd, ref velocity.x, 0);
                }
            } else
            {
                posx = Mathf.SmoothDamp(mapStart, mapStart, ref velocity.x, 0);
            }
            posy = Mathf.SmoothDamp(transform.position.y+1.5f, player.transform.position.y+1.5f, ref velocity.y, 0);
            transform.position = new Vector3(posx, transform.position.y, transform.position.z);
        }
    }
}