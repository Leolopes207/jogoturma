﻿using UnityEngine;
using System.Collections;

public class NomeBoss : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(Pause());
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator Pause()
    {
        GameObject.FindWithTag("Player").GetComponent<PlayerScript>().enabled = false;
        GameObject.FindWithTag("Boss").GetComponent<ArthurBoss>().enabled = false;
        yield return new WaitForSeconds(3);
        GameObject.FindWithTag("Player").GetComponent<PlayerScript>().enabled = true;
        GameObject.FindWithTag("Boss").GetComponent<ArthurBoss>().enabled = true;
        Destroy(this.gameObject);
    }
}
