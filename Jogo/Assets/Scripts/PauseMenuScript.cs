﻿using UnityEngine;
using System.Collections;

public class PauseMenuScript : MonoBehaviour {

    public bool isPaused = false;
    public GameObject menu;
    public float tempo;

	// Use this for initialization
	void Start () {
        menu.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = tempo;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("Cancel"))
        {
            isPaused = !isPaused;
            if (isPaused)
            {
                Time.timeScale = 0;
                menu.SetActive(isPaused);
                Cursor.visible = true;
            }
            else {
                Time.timeScale = tempo;
                menu.SetActive(isPaused);
                Cursor.visible = false;
            }
        }
	}

    public void Resumir()
    {
        isPaused = !isPaused;
        Time.timeScale = tempo;
        menu.SetActive(isPaused);
        Cursor.visible = false;
    }

    public void Sair()
    {
        Application.Quit();
    }
}
