﻿using UnityEngine;
using System.Collections;
public class PlayerScript : MonoBehaviour
{
    public Transform GroundCheck;
    public bool onground;
    public LayerMask groundtest;
    public float Speed = 10f;
    public float JumpForce;
    private float move = 0f;
    public bool isFacingLeft;
    public Animator anim;
    Rigidbody2D rigidbody2D;
    public GameObject shot;
    private float shotDelay;

    void Start()
    {
        this.rigidbody2D = this.GetComponent<Rigidbody2D>();
        shotDelay = 0;
    }

	void FixedUpdate() {
		onground = Physics2D.OverlapCircle(GroundCheck.position, 0.2f, groundtest);
		move = Input.GetAxis("Horizontal");
		if (move != 0)
		{
			if(move > 0)
			{
				if (isFacingLeft)
					Flip();
			} else
			{
				if (!isFacingLeft)
					Flip();
			}
			rigidbody2D.velocity = new Vector2(move * Speed, rigidbody2D.velocity.y);
			if(onground)
				anim.Play("PlayerAndando");
		}
		else {
			if(onground)
				anim.Play("PlayerParado");
		}

		if (Input.GetButton("Jump") && onground)
		{
			rigidbody2D.AddForce(new Vector2(rigidbody2D.velocity.x, JumpForce));
			anim.Play("Pulando");
		}

		if (Input.GetButton("Fire1") && Time.time > shotDelay)
		{
			if(transform.localScale.x / -1 > 0)
				Instantiate(shot, new Vector3(transform.position.x-1, transform.position.y, transform.position.z), Quaternion.identity);
			else
				Instantiate(shot, new Vector3(transform.position.x+1, transform.position.y, transform.position.z), Quaternion.identity);
			shotDelay = Time.time + 0.2f;
		}
	}

    void Flip()
    {
        isFacingLeft = !isFacingLeft;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}