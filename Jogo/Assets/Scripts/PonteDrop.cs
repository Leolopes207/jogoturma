﻿using UnityEngine;
using System.Collections;

public class PonteDrop : MonoBehaviour {

    public float fim;
    public float speed;
    // Use this for initialization
    void Start()
    {
    }

    void Update()
    {
        if(transform.position.y > fim)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + speed * Time.deltaTime * -1, transform.position.z);
        } else
        {
            transform.position = new Vector3(transform.position.x, 0.3f, transform.position.z);
            GameObject.FindWithTag("Player").GetComponent<PlayerScript>().enabled = true;
            this.enabled = false;
        }
    }
}
