﻿using UnityEngine;
using System.Collections;

public class EnemyWalkScript : MonoBehaviour {

    public float start;
    public float end;
    private float x;
    private float aux = -1;
    public float speed;

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void FixedUpdate() {
        x = transform.position.x;
        if (x <= start) {
            aux = 1;
            Flip();
        }
        if (x >= end) { 
            aux = -1;
            Flip();
        }
        x += speed * Time.deltaTime * aux;
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    void Flip()
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
