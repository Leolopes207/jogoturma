﻿using UnityEngine;
using System.Collections;

public class PlayerJump : MonoBehaviour {

    public Rigidbody2D rigidbody2D;
    public bool onground;
    // Use this for initialization
	void Start () {
        rigidbody2D = this.gameObject.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        onground = this.gameObject.GetComponent<PlayerScript>().onground;
        if (Input.GetButton("Jump") && onground)
        {
            rigidbody2D.AddForce(new Vector2(rigidbody2D.velocity.x, 800));
            this.gameObject.GetComponent<Animator>().Play("Pulando");
        }
    }
}
