﻿using UnityEngine;
using System.Collections;

public class BossShot : MonoBehaviour
{

    public Transform target;
    public float ProjectileSpeed = 20;
    private Transform myTransform;
    public GameObject player;
    public bool flag = false;

    void Awake()
    {
        myTransform = transform;
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Physics2D.IgnoreCollision(this.gameObject.GetComponent<BoxCollider2D>(), GameObject.FindGameObjectWithTag("Boss").GetComponent<BoxCollider2D>());
        if (player != null)
        {
            target = player.transform;
            // rotate the projectile to aim the target:
            Vector3 norTar = (target.position - transform.position).normalized;
            float angle = Mathf.Atan2(norTar.y, norTar.x) * Mathf.Rad2Deg;
            // rotate to angle
            Quaternion rotation = new Quaternion();
            rotation.eulerAngles = new Vector3(0, 0, angle - 90);
            transform.rotation = rotation;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // distance moved since last frame:
        // translate projectile in its forward direction:
        transform.Translate(new Vector3(0, 1, 0) * ProjectileSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            player.GetComponent<PlayerScript>().enabled = false;
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            GameObject.FindGameObjectWithTag("Pause").GetComponent<PauseMenuScript>().enabled = false;
            flag = true;
            StartCoroutine(Explosion());
        }
        if (!flag)
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator Explosion()
    {
        player.GetComponent<Animator>().Play("Explosao");
        yield return new WaitForSeconds(1);
        Object.Destroy(player);
    }
}