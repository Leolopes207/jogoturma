﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {

    public Rigidbody2D rigidbody;
    public float bulletSpeed;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        if (GameObject.FindGameObjectWithTag("Player").transform.localScale.x / -1 > 0)
            bulletSpeed *= -1;
    }
	
	// Update is called once per frame
	void Update () {
        rigidbody.velocity = new Vector2(bulletSpeed, rigidbody.velocity.y);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "fodac")
        {
            bulletSpeed *= -1;
        } else {
            if(collider.gameObject.tag == "Enemy" || collider.gameObject.tag == "Player")
            {
                Destroy(collider.gameObject);
                Destroy(this.gameObject);
            } else
            {
                Destroy(this.gameObject);
            }
        }
    }
}
